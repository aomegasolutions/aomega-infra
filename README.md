# aomega-infra

Infrastructure to support my personal sites. Initially a gitlab runner deployed using terraform.

* cd terraform
* terraform init
* terraform plan --var-file./terraform.tfvars
* terraform apply --var-file./terraform.tfvars
